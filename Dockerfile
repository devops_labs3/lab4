FROM python:3 as build
WORKDIR /app
ADD ./app /app
COPY requirements.txt ./
RUN pip install -r requirements.txt

FROM python:3 as prod
COPY --from=build /usr/local/lib/python3.11/site-packages/ /usr/local/lib/python3.11/site-packages/
WORKDIR /app
ADD ./app /app
ENV PYTHONUNBUFFERED 1